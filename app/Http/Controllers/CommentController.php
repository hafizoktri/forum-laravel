<?php

namespace App\Http\Controllers;

use App\Comment;
use App\thread;
use App\User;
use Auth;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $thread_id){
        
        $comment = new Comment;

        $thread = Thread::find($thread_id);

        $comment->user_id = auth()->id();
        $comment->thread()->associate($thread);
        $comment->comment = $request->comment;

        $comment->save();

        return redirect()->route('thread.show', [$thread->id]);
    }

}
