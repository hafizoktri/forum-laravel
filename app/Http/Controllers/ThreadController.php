<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\User;
use App\Thread;
use App\Comment;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    public function index(){
        $threads = Thread::all();

        return view('home', compact('threads'));
    }

    public function create(){
        return view('create');
    }

    public function store(Request $request){
        $op = new Thread;

        $op->user_id = auth()->id();
        $op->title = $request->title;
        $op->content = $request->content;

        $op->save();

        return redirect('/thread/'.$op->id);
    }

    public function show($id){
        $thread = Thread::findOrFail($id);

        $comments = DB::table('threads')->where('threads.id', $id)
                    ->join('comments', 'comments.thread_id', '=', 'threads.id')
                    ->join('users', 'users.id', '=', 'comments.user_id')->get();

        return view('thread', compact('thread', 'comments'));
    }
}
