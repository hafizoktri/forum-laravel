<?php

namespace App\Http\Controllers;

use Auth;
use App\User;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(){
        return view('auth.login');
    }    

    public function loginpost(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/');
            dd($request);
        }
        return back()->withInput();
    }

    public function logout(){
        Auth::logout();

        return redirect('/');
    }
}
