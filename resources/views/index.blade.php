<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <header class="p-3 d-flex">
        <div>
            <h2><a href="/">Home</a></h2>
            
            @if (auth()->check())
                <p>Welcome, {{Auth::user()->name}}</p>
                <a href="{{route('logout')}}" class="btn btn-danger rounded-0">Logout</a>
            @else
                <a href="{{route('login')}}" class="btn btn-primary rounded-0">Login</a>
                <a href="{{route('signup')}}" class="btn btn-success rounded-0">Sign Up</a>
            @endif
        </div>
    </header>

    @yield('content')

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>

</html>