@extends('index')

@section('title')
    Create new thread
@endsection

@section('content')
    <div class="container py-2">
        <h2>Create new Thread</h2>

        <div class="card w-75 rounded-0">

            <div class="card-body">
                <form action="{{route('thread.store')}}" method="post" class="">
                    {{ csrf_field() }}
                
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="content" cols="10" rows="5" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success rounded-0">Send</button>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection