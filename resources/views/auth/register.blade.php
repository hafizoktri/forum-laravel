@extends('index')

@section('title')
    Register New User
@endsection

@section('content')
    <section class="container">
        <h2>Register New</h2>

        <div class="card w-50 rounded-0">
            <div class="card-body">

                <form action="{{route('signup')}}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Username</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>

                    <button type="submit" class="btn btn-toolbar rounded-0">Sign Up</button>
                </form>

            </div>
        </div>
    </section>
@endsection