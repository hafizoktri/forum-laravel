@extends('index')

@section('title')
    Login
@endsection

@section('content')
    <div class="container">
        <h2>Login</h2>

        <section>
            <div class="card w-50 rounded-0">
                <div class="card-body">

                    <form action=""{{route('login')}} method="post" class="">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-success rounded-0">Login</button>
                        </div>

                        <div class="text-center">
                            <a href="/register">Register new user</a>
                        </div>
                    </form>

                </div>
            </div>
        </section>
    </div>
@endsection