@extends('index')

@section('title')
    Forum with laravel
@endsection

@section('content')
    <div class="container py-2">

        <section>
            <div class="">
                <div class="float-right">
                    <a href="/create" class="btn btn-primary rounded-0">Create new thread</a>
                </div>
        
                <div>
                    <h2>Forum with laravel</h2>
                </div>
            </div>
        </section>

        <section>
            @foreach ($threads as $thread)
                <div class="card w-75 my-3">
                    <div class="card-body">
                    
                    <a href="{{route('thread.show', $thread->id)}}" class="text-decoration-none">
                        <h4>{{$thread->title}}</h4>
                    </a>
                    
                    <p>{{$thread->user->name}}</p>
                        
                    <hr>

                    <article>
                        {{$thread->content}}
                    </article>
                    </div>
                </div>     
            @endforeach
        </section>
    </div>
@endsection