@extends('index')

@section('title')
    {{$thread->title}}
@endsection

@section('content')
    <section class="container py-2">

        <div class="card w-75 rounded-0">
            <div class="card-body">
                <h4>{{$thread->title}}</h4>

                <p class="font-weight-bold text-dark">{{$thread->user->name}}</p>
                <hr>

                <article>
                    {{$thread->content}}
                </article>

                <a href="#" class="float-right">Reply</a>
            </div>
        </div>

        @foreach ($comments as $comment)
        <div class="card w-75 mt-4 rounded-0">
            {{-- comment section --}}
                <div class="card-body">
                <p class="font-weight-bold text-dark">{{$comment->name}}</p>

                <hr>

                <p>{{$comment->comment}}</p>
            </div>
        </div>
        @endforeach

        <div class="card w-75 mt-4 rounded-0">
            {{-- comment form --}}

            <div class="card-body">
                <form action="{{route('comment.store', $thread->id)}}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="comment">Insert comment</label>
                        <textarea name="comment" id="" cols="10" rows="5" class="form-control"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success rounded-0">Send</button>
                </form>
            </div>
        </div>

    </section>
@endsection