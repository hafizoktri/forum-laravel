<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Thread
Route::get('/', 'ThreadController@index');
Route::get('/create', 'ThreadController@create')->name('thread.create');
Route::post('/store', 'ThreadController@store')->name('thread.store');
Route::get('/thread/{thread}/', 'ThreadController@show')->name('thread.show');

//Comment
Route::post('/comment/{thread_id}', 'CommentController@store')->name('comment.store');

//Auth route
Route::get('/login', 'LoginController@login')->name('login');; 
Route::post('/login', 'LoginController@loginpost'); 
Route::get('/logout', 'LoginController@logout')->name('logout');; 

Route::get('/register', 'RegisterController@index')->name('signup');
Route::post('/register', 'RegisterController@register')->name('register');